// Setup env variables
require('dotenv').config()

// Setup express
const express = require("express")
const app = express()
const port = process.env.PORT || 3353

// Setup body-parser
const bodyParser = require("body-parser")
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

// Setup db
const db = require("./routes/db")
db.createDB()


// Authentication routes
const auth = require("./routes/auth")
app.use("/auth", auth)

// Data routes
const data = require("./routes/data")
app.use("/data", data)


// Run server
app.listen(port, () => {
    console.log("[Server] Running on port " + port)
})