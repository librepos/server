# LibrePOS Backend Server
This is the repository for the LibrePOS backend server.

By default, it runs on port 80, however this can be overriden with the PORT env variable.