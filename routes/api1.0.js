// --- SETUP ---

// Express
const express = require("express")
const router = express.Router()


// --- API ---

// User management - DUMMY
router.use("/users", require("./1.0/users"))


// --- EXPORT ---
module.exports = router