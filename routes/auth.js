// Setup express
const express = require("express")
const router = express.Router()

// Setup bcrypt
const bcrypt = require("bcrypt")
const saltRounds = 10

// Setup mysql
const db = require("./db")


// Login
router.post("/login", (req, res) => {

    // Get request (user, pass)
    let request = req.body

    // Get data
    db.getData("users", "*").then(results => {

        // Check if fields are empty
        if (request.user && request.pass) {
            
            // Get user...
            let user = results.find(u => u.username === request.user)

            // ... and checks if they exit
            if (user != undefined) {

                // Match passwords
                if (bcrypt.compareSync(request.pass, user.pass)) {
                    res.json({"user": user.username, "logged": true})

                    // Add key to db
                    db.loginKey(request.key, request.user)

                // Invalid passwords
                } else {
                    res.json({"logged": false})
                }

            // No user
            } else {
                res.json({"logged": false})
            }

        // Not full request
        } else {
            res.json({"logged": false})
        }
    })
})


// Register
router.post("/register", (req, res) => {

    // Get request (user, pass)
    let request = req.body

    // Check if fields are empty
    if (request.user != undefined && request.pass != undefined) {

        // Hash password
        bcrypt.hash(request.pass, saltRounds, (err, hash) => {
            db.addData("users", [request.user, hash, request.key])
        })
    }

    // Stop request
    res.end()
})


// Export
module.exports = router