// env variables
const server = process.env.DB_SERVER || "localhost"
const dbUser = process.env.DB_USER || "librepos"
const dbPass = process.env.DB_PASS || "librepos"
const dbPort = process.env.DB_PORT || 3306


// Setup mysql
const mysql = require("mysql")

// Connection
var con = mysql.createConnection({
    host: server,
    user: dbUser,
    password: dbPass,
    port: dbPort,
    database: "librepos"
})


// Create db
function createDB() {
    con.query("CREATE TABLE IF NOT EXISTS users (username VARCHAR(63) UNIQUE, pass VARCHAR(255), userkey CHAR(16));", (err, result) => {if (err) throw err})
    con.query("CREATE TABLE IF NOT EXISTS stock (id INT AUTO_INCREMENT primary key NOT NULL, name VARCHAR(255) NOT NULL, stock SMALLINT(3) DEFAULT 0 NOT NULL, price DOUBLE(6,2) NOT NULL);", (err, result) => {if (err) throw err})
}

// Get data
async function getData(table, value) {

    // Query
    let query = "SELECT " + value + " FROM " + table + ";"

    // Async => sync
    return new Promise((res, rej) => {

        // Query db
        con.query(query, (err, results) => {
            if (err) throw err
            res(results)
        })
    })
}

// Insert data
function addData(table, values) {

    // Converts values into correctly-formatted list
    values = values.map(v => `'${v}'`)
    values = "(" + values.join(", ") + ");"

    // Alters for stock table
    if (table == "stock") {
        table += " (name, stock, price)"
    }

    // Query db
    con.query("INSERT INTO " + table + " VALUES " + values, (err, result) => {
        if (err) throw err
    })
}

// Delete data
function deleteData(table, column, value) {
    
    // Query
    let query = "DELETE FROM " + table + " WHERE " + column + "='" + value + "';"

    // Query db
    con.query(query, (err, results) => {
        if (err) throw err
    })
}

// Set login key
function loginKey(key, user) {

    // Query
    let query = "UPDATE users SET userkey='" + key + "' WHERE username='" + user + "';"

    // Query db
    con.query(query, (err, results) => {
        if (err) throw err
    })
}


// Export functions
module.exports = {
    createDB, getData, addData, deleteData, loginKey
}