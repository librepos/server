// Setup express
const express = require("express")
const db = require("./db")
const router = express.Router()


// Manipulate products
router.post("/products/:action", (req, res) => {

    // Get request (user, pass, product)
    let request = req.body

    // Get data
    db.getData("users", "*").then(results => {

        // Check if fields are empty
        if (request.user && request.key) {
            
            // Get user...
            let user = results.find(u => u.username === request.user)

            // ... and checks if they exit
            if (user != undefined) {

                // Match keys
                if (request.key === user.userkey) {
                    
                    // Get stock inventory
                    db.getData("stock", "*").then(stock => {

                        // Get all
                        if (req.params.action == "get") {
                            res.send(stock)

                        // Add
                        } else if (req.params.action == "add") {
                            db.addData("stock", request.product)
                            res.end()

                        // Delete
                        } else if (req.params.action == "delete") {
                            db.deleteData("stock", "id", request.id)
                            res.end()
                        }
                    })

                // Invalid passwords
                } else {
                    res.json({"logged": false})
                }

            // No user
            } else {
                res.json({"logged": false})
            }

        // Not full request
        } else {
            res.json({"logged": false})
        }
    })
})



// Export
module.exports = router