// --- SETUP ---

// ENV Variables
require("dotenv").config()
const DEV = process.env.NODE_ENVIRONMENT === "production" ? false : true || false

// Express
const express = require("express")
const router = express.Router()
const bodyParser = require("body-parser")
router.use(bodyParser.urlencoded({ extended: true }))


// --- DUMMY INFORMATION ---
var users = []


// --- USERS API ---

// Get specific user
router.get("/get/:method/:user", (req, res) => {

    // Map users to locate specific user
    const result = users.map((user) => {

        // By ID
        if (req.params.method == "id") {
            if (req.params.user == user.id) return user

        // By username
        } else if (req.params.method == "username") {
            if (req.params.user == user.username) return user
        }
    })[0]

    // Send result
    res.send((result != null) ? result : 404)
})

// Get all users
router.get("/get", (req, res) => {
    res.send(users)
})

// Create user
router.post("/create", (req, res) => {

    // Throw error if body is empty
    try {
        if (Object.keys(req.body).length === 0) throw 42

        // Set variables
        let newId = crypto.randomUUID()
        let newUsername = req.body.username.replaceAll("'", "")
        let newPassword = req.body.password.replaceAll("'", "")
        let newName = req.body.name.replaceAll("'", "")

        // Create user
        let newUser = { id: newId, username: newUsername, name: newName, password: newPassword }
        DEV && console.log(newUser)

        // Submit user
        users.push(newUser)
        res.sendStatus(201)

    } catch {
        res.sendStatus(400)
    }
})


// --- EXPORT ---
module.exports = router