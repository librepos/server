# syntax=docker/dockerfile:1

#
# Dockerfile
# Builds the container
#

# Imports NodeJS lightweight container
FROM node:18-alpine

# Copys everying across & installs it
WORKDIR /app
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install --production
COPY . .

# Sets it to production
ENV NODE_ENV="production"

# Runs index
CMD [ "node", "index" ]