const version = "v0.0.1"

// --- SETUP ---

// ENV Variables
require("dotenv").config()
const PORT = process.env.PORT || 80
const DEV = process.env.NODE_ENVIRONMENT === "production" ? false : true || 
false
const VERSION = DEV ? version + "-DEV" : version

// Express
const express = require("express")
const app = express()

// Pug
app.set("views", "./views")
app.set("view engine", "pug")
app.use(express.static("./public"))

app.get("/", (req, res) => {
    res.render("status", { version: VERSION })
})

app.get("/status", (req, res) => {
    res.send(version + "\n")
})


// --- API ---

// Debug log
app.use((req, res, next) => {
    DEV && console.log("[R] Request made on " + req.url)
    next()
})

// v1.0
app.use("/api/1.0/", require("./routes/api1.0"))


// --- RUN ---
app.listen(PORT, () => {
    DEV && console.log("[S] Server running on PORT: " + PORT)
})